<?php /* Smarty version 3.1.24, created on 2017-11-24 16:18:08
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/admin.dashboard.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:6737372185a18464022c2c3_70740865%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8f93c5cce39cbbabf0504e8e0f82d28597955e52' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/admin.dashboard.tpl',
      1 => 1446346350,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6737372185a18464022c2c3_70740865',
  'variables' => 
  array (
    'insights' => 0,
    'system' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a1846402449b0_13473182',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a1846402449b0_13473182')) {
function content_5a1846402449b0_13473182 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '6737372185a18464022c2c3_70740865';
?>
<div class="row">
    <div class="col-sm-4">
        <div class="stat-panel">
            <div class="stat-cell">
                <i class="fa fa-user bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['users'];?>
</span><br>
                <span class="text-bg"><?php echo __("Users");?>
</span><br>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users"><?php echo __("Manage Users");?>
</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="stat-panel">
            <div class="stat-cell">
                <i class="fa fa-male bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['users_males'];?>
</span><br>
                <span><?php echo $_smarty_tpl->tpl_vars['insights']->value['users_males_percent'];?>
%</span><br>
                <span class="text-bg"><?php echo __("Males");?>
</span><br>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="stat-panel">
            <div class="stat-cell">
                <i class="fa fa-female bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['users_females'];?>
</span><br>
                <span><?php echo $_smarty_tpl->tpl_vars['insights']->value['users_females_percent'];?>
%</span><br>
                <span class="text-bg"><?php echo __("Females");?>
</span><br>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="stat-panel danger">
            <div class="stat-cell">
                <i class="fa fa-minus-circle bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['banned'];?>
</span><br>
                <span class="text-bg"><?php echo __("Banned");?>
</span><br>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users/banned"><?php echo __("Manage Banned");?>
</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="stat-panel warning">
            <div class="stat-cell">
                <i class="fa fa-envelope bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['not_activated'];?>
</span><br>
                <span class="text-bg"><?php echo __("Not Activated");?>
</span><br>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users"><?php echo __("Manage Users");?>
</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="stat-panel info">
            <div class="stat-cell">
                <i class="fa fa-clock-o bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['online'];?>
</span><br>
                <span class="text-bg"><?php echo __("Online");?>
</span><br>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/users/online"><?php echo __("Manage Online");?>
</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="stat-panel success">
            <div class="stat-cell">
                <i class="fa fa-newspaper-o bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['posts'];?>
</span><br>
                <span class="text-bg"><?php echo __("Posts");?>
</span><br>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/reports"><?php echo __("Manage Reports");?>
</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="stat-panel success">
            <div class="stat-cell">
                <i class="fa fa-comments bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['comments'];?>
</span><br>
                <span class="text-bg"><?php echo __("Comments");?>
</span><br>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/reports"><?php echo __("Manage Reports");?>
</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="stat-panel">
            <div class="stat-cell">
                <i class="fa fa-flag bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['pages'];?>
</span><br>
                <span class="text-bg"><?php echo __("Pages");?>
</span><br>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/pages"><?php echo __("Manage Pages");?>
</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="stat-panel">
            <div class="stat-cell">
                <i class="fa fa-users bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['groups'];?>
</span><br>
                <span class="text-bg"><?php echo __("Groups");?>
</span><br>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/groups"><?php echo __("Manage Groups");?>
</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="stat-panel info">
            <div class="stat-cell">
                <i class="fa fa-comments-o bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['messages'];?>
</span><br>
                <span class="text-bg"><?php echo __("Messages");?>
</span><br>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="stat-panel info">
            <div class="stat-cell">
                <i class="fa fa-globe bg-icon"></i>
                <span class="text-xlg"><?php echo $_smarty_tpl->tpl_vars['insights']->value['notifications'];?>
</span><br>
                <span class="text-bg"><?php echo __("Notifications");?>
</span><br>
            </div>
        </div>
    </div>
</div><?php }
}
?>