<?php /* Smarty version 3.1.24, created on 2017-11-24 16:03:08
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/__feeds_post.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:14156919775a1842bc170ab1_79470393%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9120da69427a14f899ed3ac7834cca589e185fdd' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/__feeds_post.tpl',
      1 => 1451867070,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14156919775a1842bc170ab1_79470393',
  'variables' => 
  array (
    'single' => 0,
    'post' => 0,
    'user' => 0,
    'system' => 0,
    'photo' => 0,
    '_snippet' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a1842bc23d0a8_33682026',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a1842bc23d0a8_33682026')) {
function content_5a1842bc23d0a8_33682026 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '14156919775a1842bc170ab1_79470393';
if (!$_smarty_tpl->tpl_vars['single']->value) {?><li><?php }?>
    <!-- post -->
    <div class="post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">

        <!-- post body -->
        <div class="post-body">
            <!-- post header -->
            <div class="post-header">
                <!-- post picture -->
                <div class="post-avatar">
                    <a class="post-avatar-picture" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_url'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['post']->value['post_picture'];?>
);">
                    </a>
                </div>
                <!-- post picture -->

                <!-- post meta -->
                <div class="post-meta">
                    <!-- post author name & menu -->
                    <div>
                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                            <!-- post menu -->
                            <?php if ($_smarty_tpl->tpl_vars['post']->value['user_type'] == "user") {?>
                                <?php if ($_smarty_tpl->tpl_vars['post']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'] || ($_smarty_tpl->tpl_vars['post']->value['in_group'] && $_smarty_tpl->tpl_vars['post']->value['group_admin'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'])) {?>
                                    <div class="pull-right flip">
                                        <button type="button" class="close js_delete-post"><span>&times;</span></button>
                                    </div>
                                <?php } else { ?>
                                    <div class="pull-right flip dropdown">
                                        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="dropdown-menu post-dropdown-menu">
                                            <li>
                                                <a href="#" class="js_hide-post">
                                                    <div class="action">
                                                        <i class="fa fa-eye-slash fa-fw"></i> <?php echo __("Hide this post");?>

                                                    </div>
                                                    <div class="action-desc"><?php echo __("See fewer posts like this");?>
</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="js_hide-author" data-author-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
" data-author-name="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_name'];?>
">
                                                    <div class="action">
                                                        <i class="fa fa-minus-circle fa-fw"></i> <?php echo __("Unfollow");?>
 <?php echo get_firstname($_smarty_tpl->tpl_vars['post']->value['user_fullname']);?>

                                                    </div>
                                                    <div class="action-desc"><?php echo __("Stop seeing posts but stay friends");?>
</div>
                                                </a>
                                            </li>
                                            <li role="presentation" class="divider"></li>
                                            <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_group'] < 3) {?>
                                            <li>
                                                <a href="#" class="js_delete-post">
                                                    <div class="action no-icon"><?php echo __("Delete post");?>
</div>
                                                </a>
                                            </li>
                                            <?php } else { ?>
                                            <li>
                                                <a href="#" class="js_report-post">
                                                    <div class="action no-icon"><?php echo __("Report post");?>
</div>
                                                </a>
                                            </li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                <?php }?>
                            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['user_type'] == "page") {?>
                                <?php if ($_smarty_tpl->tpl_vars['post']->value['page_admin'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                    <div class="pull-right flip">
                                        <button type="button" class="close js_delete-post"><span>&times;</span></button>
                                    </div>
                                <?php } else { ?>
                                    <div class="pull-right flip dropdown">
                                        <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></i>
                                        <ul class="dropdown-menu post-dropdown-menu">
                                            <li>
                                                <a href="#" class="js_hide-post">
                                                    <div class="action">
                                                        <i class="fa fa-eye-slash fa-fw"></i> <?php echo __("Hide this post");?>

                                                    </div>
                                                    <div class="action-desc"><?php echo __("See fewer posts like this");?>
</div>
                                                </a>
                                            </li>
                                            <li role="presentation" class="divider"></li>
                                            <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_group'] < 3) {?>
                                            <li>
                                                <a href="#" class="js_delete-post">
                                                    <div class="action no-icon"><?php echo __("Delete post");?>
</div>
                                                </a>
                                            </li>
                                            <?php } else { ?>
                                            <li>
                                                <a href="#" class="js_report-post">
                                                    <div class="action no-icon"><?php echo __("Report post");?>
</div>
                                                </a>
                                            </li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                <?php }?>
                            <?php }?>
                            <!-- post menu -->
                        <?php }?>

                        <!-- post author name -->
                        <span class="js_user-popover" data-type="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_type'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_id'];?>
">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['post_author_name'];?>
</a>
                        </span>
                        <?php if ($_smarty_tpl->tpl_vars['post']->value['post_author_verified']) {?>
                        <i data-toggle="tooltip" data-placement="top" title="<?php echo __("Verified profile");?>
" class="fa fa-check verified-badge"></i>
                        <?php }?>
                        <!-- post author name -->

                        <!-- post type meta -->
                        <span class="post-title">
                        <?php if ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "shared") {?>
                            <?php echo __("shared");?>
 
                            <span class="js_user-popover" data-type="<?php echo $_smarty_tpl->tpl_vars['post']->value['origin']['user_type'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['post']->value['origin']['user_id'];?>
">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['origin']['post_author_url'];?>
">
                                    <?php echo $_smarty_tpl->tpl_vars['post']->value['origin']['post_author_name'];?>

                                </a><?php echo __("'s");?>

                            </span> 
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['origin']['post_id'];?>
">
                            
                            <?php if ($_smarty_tpl->tpl_vars['post']->value['origin']['post_type'] == 'photos') {?>
                                <?php if ($_smarty_tpl->tpl_vars['post']->value['origin']['photos_num'] > 1) {
echo __("photos");
} else {
echo __("photo");
}?>
                            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['origin']['post_type'] == 'media') {?>
                                <?php if ($_smarty_tpl->tpl_vars['post']->value['origin']['media']['media_type'] != "soundcloud") {?>
                                    <?php echo __("video");?>

                                <?php } else { ?>
                                    <?php echo __("song");?>

                                <?php }?>
                            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['origin']['post_type'] == 'link') {?>
                                <?php echo __("link");?>

                            <?php } else { ?>
                                <?php echo __("post");?>

                            <?php }?>
                            </a>

                        <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "link") {?>
                            <?php echo __("shared a link");?>


                        <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "photos") {?>
                            <?php if ($_smarty_tpl->tpl_vars['post']->value['photos_num'] == 1) {?>
                                <?php echo __("added a photo");?>

                            <?php } else { ?>
                                <?php echo __("added");?>
 <?php echo $_smarty_tpl->tpl_vars['post']->value['photos_num'];?>
 <?php echo __("photos");?>

                            <?php }?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "video") {?>
                            <?php echo __("added a video");?>


                        <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "avatar") {?>
                            <?php if ($_smarty_tpl->tpl_vars['post']->value['user_gender'] == "M") {?>
                            <?php echo __("updated his profile picture");?>

                            <?php } else { ?>
                            <?php echo __("updated her profile picture");?>

                            <?php }?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "cover") {?>
                            <?php if ($_smarty_tpl->tpl_vars['post']->value['user_gender'] == "M") {?>
                            <?php echo __("updated his cover photo");?>

                            <?php } else { ?>
                            <?php echo __("updated her cover photo");?>

                            <?php }?>
                            
                        <?php }?>
                        </span>
                        <!-- post type meta -->
                    </div>
                    <!-- post author name & menu -->

                    <!-- post time & location & privacy -->
                    <div class="post-time">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
" class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['post']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['time'];?>
</a>

                        <?php if ($_smarty_tpl->tpl_vars['post']->value['location']) {?>
                        ·
                        <i class="fa fa-map-marker"></i> <span><?php echo $_smarty_tpl->tpl_vars['post']->value['location'];?>
</span>
                        <?php }?>

                        - 
                        <?php if ($_smarty_tpl->tpl_vars['post']->value['privacy'] == "friends") {?>
                            <i class="fa fa-users" data-toggle="tooltip" data-placement="top" title="<?php echo __("Shared with");?>
: <?php echo __("Friends");?>
"></i>
                        <?php } else { ?>
                            <i class="fa fa-globe" data-toggle="tooltip" data-placement="top" title="<?php echo __("Shared with");?>
: <?php echo __("Public");?>
"></i>
                        <?php }?>
                    </div>
                    <!-- post time & location & privacy -->
                </div>
                <!-- post meta -->
            </div>
            <!-- post header -->

            <!-- post text -->
            <div class="post-text"><?php echo $_smarty_tpl->tpl_vars['post']->value['text'];?>
</div>
            <!-- post text -->

            <?php if ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "avatar") {?>
            <div class="pg_wrapper">
                <div class="pg_1x">
                    <a href="#" class="js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_picture'];?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['user_picture'];?>
">
                    </a>
                </div>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "cover") {?>
            <div class="pg_wrapper">
                <div class="pg_1x">
                    <a href="#" class="js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['user_cover'];?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['user_cover'];?>
">
                    </a>
                </div>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "photos" && $_smarty_tpl->tpl_vars['post']->value['photos']) {?>
            <div class="mt10 clearfix">
                <div class="pg_wrapper">
                <?php if ($_smarty_tpl->tpl_vars['post']->value['photos_num'] == 1) {?>
                    <div class="pg_1x">
                        <a href="#" class="js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['source'];?>
">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['source'];?>
">
                        </a>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['photos_num'] == 2) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['post']->value['photos'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['photo']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->value) {
$_smarty_tpl->tpl_vars['photo']->_loop = true;
$foreach_photo_Sav = $_smarty_tpl->tpl_vars['photo'];
?>
                        <div class="pg_2x">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['photo']->value['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['photo']->value['source'];?>
');"></a>
                        </div>
                    <?php
$_smarty_tpl->tpl_vars['photo'] = $foreach_photo_Sav;
}
?>
                <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['photos_num'] == 3) {?>
                    <div class="pg_3x">
                        <div class="pg_2o3">
                            <div class="pg_2o3_in">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['source'];?>
');"></a>
                            </div>
                        </div>
                        <div class="pg_1o3">
                            <div class="pg_1o3_in">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][1]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][1]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][1]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][1]['source'];?>
');"></a>
                            </div>
                            <div class="pg_1o3_in">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][2]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][2]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][2]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][2]['source'];?>
');"></a>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="pg_4x">
                        <div class="pg_2o3">
                            <div class="pg_2o3_in">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][0]['source'];?>
');"></a>
                            </div>
                        </div>
                        <div class="pg_1o3">
                            <div class="pg_1o3_in">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][1]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][1]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][1]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][1]['source'];?>
');"></a>
                            </div>
                            <div class="pg_1o3_in">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][2]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][2]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][2]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][2]['source'];?>
');"></a>
                            </div>
                            <div class="pg_1o3_in">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][3]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][3]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][3]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['photos'][3]['source'];?>
');">
                                    <?php if ($_smarty_tpl->tpl_vars['post']->value['photos_num'] > 4) {?>
                                    <span class="more">+<?php echo $_smarty_tpl->tpl_vars['post']->value['photos_num']-4;?>
</span>
                                    <?php }?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php }?>
                </div>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "media" && $_smarty_tpl->tpl_vars['post']->value['media']) {?>
            <div class="mt10">
                <?php if ($_smarty_tpl->tpl_vars['post']->value['media']['media_type'] == "youtube") {?>
                    <div class="post-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_uid'];?>
" allowfullscreen=""></iframe>
                        </div>
                        <div class="post-media-meta">
                            <a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_title'];?>
</a>
                            <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_text'];?>
</div>
                            <div class="source">youtube.com</div>
                        </div>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['media']['media_type'] == "vimeo") {?>
                    <div class="post-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="http://player.vimeo.com/video/<?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_uid'];?>
"></iframe>
                        </div>
                        <div class="post-media-meta">
                            <a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_title'];?>
</a>
                            <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_text'];?>
</div>
                            <div class="source">vimeo.com</div>
                        </div>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['media']['media_type'] == "soundcloud") {?>
                    <div class="post-media">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe height="450" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_uid'];?>
&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                        </div>
                        <div class="post-media-meta">
                            <a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_title'];?>
</a>
                            <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['post']->value['media']['source_text'];?>
</div>
                            <div class="source">soundcloud.com</div>
                        </div>
                    </div>
                <?php }?>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "link" && $_smarty_tpl->tpl_vars['post']->value['link']) {?>
            <div class="mt10">
                <div class="post-media">
                    <?php if ($_smarty_tpl->tpl_vars['post']->value['link']['source_thumbnail']) {?>
                        <div class="post-media-image">
                            <div style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['post']->value['link']['source_thumbnail'];?>
');"></div>
                        </div>
                    <?php }?>
                    <div class="post-media-meta">
                        <a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['link']['source_url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['post']->value['link']['source_title'];?>
</a>
                        <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['post']->value['link']['source_text'];?>
</div>
                        <div class="source"><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['post']->value['link']['source_host'], 'UTF-8');?>
</div>
                    </div>
                </div>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "video" && $_smarty_tpl->tpl_vars['post']->value['video']) {?>
                <video controls>
                    <source src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value['video']['source'];?>
" type="video/mp4">
                    <?php echo __("Your browser does not support HTML5 video");?>

                </video>
            <?php } elseif ($_smarty_tpl->tpl_vars['post']->value['post_type'] == "shared" && $_smarty_tpl->tpl_vars['post']->value['origin']) {?>
            <?php if ($_smarty_tpl->tpl_vars['_snippet']->value) {?>
            <span class="text-link js_show-attachments"><?php echo __("Show Attachments");?>
</span>
            <?php }?>
            <div class="mt10 <?php if ($_smarty_tpl->tpl_vars['_snippet']->value) {?>x-hidden<?php }?>">
                <div class="post-media">
                    <div class="post-media-meta">
                        <?php echo $_smarty_tpl->getSubTemplate ('__feeds_post_shared.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('origin'=>$_smarty_tpl->tpl_vars['post']->value['origin']), 0);
?>

                    </div>
                </div>
            </div>
            <?php }?>

            <!-- post actions & stats -->
            <div class="post-actions">
                <!-- post actions -->
                <?php if ($_smarty_tpl->tpl_vars['post']->value['i_like']) {?>
                    <span class="text-link js_unlike-post"><?php echo __("Unlike");?>
</span> - 
                <?php } else { ?>
                    <span class="text-link js_like-post"><?php echo __("Like");?>
</span> - 
                <?php }?>
                <span class="text-link js_comment"><?php echo __("Comment");?>
</span>
                <?php if ($_smarty_tpl->tpl_vars['post']->value['privacy'] == "public") {?>
                     - 
                    <span class="text-link js_share"><?php echo __("Share");?>
</span>
                <?php }?>
                <!-- post actions -->

                <!-- post stats -->
                <span class="post-stats-alt <?php if ($_smarty_tpl->tpl_vars['post']->value['likes'] == 0 && $_smarty_tpl->tpl_vars['post']->value['comments'] == 0 && $_smarty_tpl->tpl_vars['post']->value['shares'] == 0) {?>x-hidden<?php }?>">
                    <i class="fa fa-thumbs-o-up"></i> <span class="js_post-likes-num"><?php echo $_smarty_tpl->tpl_vars['post']->value['likes'];?>
</span> 
                    <i class="fa fa-comments"></i> <span class="js_post-comments-num"><?php echo $_smarty_tpl->tpl_vars['post']->value['comments'];?>
</span> 
                    <i class="fa fa-share"></i> <span class="js_post-shares-num"><?php echo $_smarty_tpl->tpl_vars['post']->value['shares'];?>
</span>
                </span>
                <!-- post stats -->
            </div>
            <!-- post actions & stats -->
        </div>
        <!-- post body -->

        <!-- post footer -->
        <div class="post-footer <?php if ($_smarty_tpl->tpl_vars['post']->value['likes'] == 0 && $_smarty_tpl->tpl_vars['post']->value['comments'] == 0 && $_smarty_tpl->tpl_vars['post']->value['shares'] == 0) {?>x-hidden<?php }?>">

            <!-- post stats (likes & shares) -->
            <div class="post-stats clearfix <?php if ($_smarty_tpl->tpl_vars['post']->value['likes'] == 0 && $_smarty_tpl->tpl_vars['post']->value['shares'] == 0) {?>x-hidden<?php }?>">
                <!-- shares -->
                <div class="pull-right flip js_post-shares <?php if ($_smarty_tpl->tpl_vars['post']->value['shares'] == 0) {?>x-hidden<?php }?>">
                    <i class="fa fa-share"></i> 
                    <span class="text-link" data-toggle="modal" data-url="posts/who_shares.php?post_id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
">
                        <?php echo $_smarty_tpl->tpl_vars['post']->value['shares'];
echo __("shares");?>

                    </span>
                </div>
                <!-- shares -->

                <!-- likes -->
                <span class="js_post-likes <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value['likes'];
$_tmp1=ob_get_clean();
if ($_tmp1 == 0) {?>x-hidden<?php }?>">
                    <i class="fa fa-thumbs-o-up"></i> <span class="text-link" data-toggle="modal" data-url="posts/who_likes.php?post_id=<?php echo $_smarty_tpl->tpl_vars['post']->value['post_id'];?>
"><span class="js_post-likes-num"><?php echo $_smarty_tpl->tpl_vars['post']->value['likes'];?>
</span> <?php echo __("people");?>
</span> <?php echo __("like this");?>

                </span>
                <!-- likes -->
            </div>
            <!-- post stats (likes & shares) -->

            <!-- comments -->
            <?php echo $_smarty_tpl->getSubTemplate ('__feeds_post.comments.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            <!-- comments -->
        </div>
        <!-- post footer -->

    </div>
    <!-- post -->
<?php if (!$_smarty_tpl->tpl_vars['single']->value) {?></li><?php }
}
}
?>