<?php /* Smarty version 3.1.24, created on 2017-11-24 16:07:46
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/ajax.who_likes.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:19345730515a1843d2a21c54_30648056%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be853031b2f2135ee70f9f123c17b6a135ff93ae' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/ajax.who_likes.tpl',
      1 => 1447601976,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19345730515a1843d2a21c54_30648056',
  'variables' => 
  array (
    'users' => 0,
    '_user' => 0,
    'system' => 0,
    'get' => 0,
    'id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a1843d2a46e80_74557857',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a1843d2a46e80_74557857')) {
function content_5a1843d2a46e80_74557857 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '19345730515a1843d2a21c54_30648056';
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title"><?php echo __("People Who Like This");?>
</h5>
</div>
<div class="modal-body">
    <ul>
        <?php
$_from = $_smarty_tpl->tpl_vars['users']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['_user'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['_user']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
$_smarty_tpl->tpl_vars['_user']->_loop = true;
$foreach__user_Sav = $_smarty_tpl->tpl_vars['_user'];
?>
        <?php echo $_smarty_tpl->getSubTemplate ('__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_connection'=>$_smarty_tpl->tpl_vars['_user']->value["connection"]), 0);
?>

        <?php
$_smarty_tpl->tpl_vars['_user'] = $foreach__user_Sav;
}
?>
    </ul>

    <?php if (count($_smarty_tpl->tpl_vars['users']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
    <!-- see-more -->
    <div class="alert alert-info see-more js_see-more" data-get="<?php echo $_smarty_tpl->tpl_vars['get']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
        <span><?php echo __("See More");?>
</span>
        <div class="loader loader_small x-hidden"></div>
    </div>
    <!-- see-more -->
    <?php }?>
    
</div>
<?php }
}
?>