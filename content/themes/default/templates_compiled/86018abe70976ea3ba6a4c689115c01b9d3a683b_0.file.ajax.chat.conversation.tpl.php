<?php /* Smarty version 3.1.24, created on 2017-11-24 16:22:11
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/ajax.chat.conversation.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:10963210845a18473386fdd4_81274238%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '86018abe70976ea3ba6a4c689115c01b9d3a683b' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/ajax.chat.conversation.tpl',
      1 => 1449082290,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10963210845a18473386fdd4_81274238',
  'variables' => 
  array (
    'conversation' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a18473389ce43_92255921',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a18473389ce43_92255921')) {
function content_5a18473389ce43_92255921 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '10963210845a18473386fdd4_81274238';
?>
<div class="panel panel-default panel-messages" data-cid=<?php echo $_smarty_tpl->tpl_vars['conversation']->value['conversation_id'];?>
>
    <div class="panel-heading clearfix">
        <div class="pull-right flip">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-default js_chat-start">
                    <i class="fa fa-comment-o"></i>
                    <?php echo __("New Message");?>

                </button>
                <button type="button" class="btn btn-default js_delete-conversation">
                    <i class="fa fa-trash-o"></i>
                    <?php echo __("Delete");?>

                </button>
            </div>
        </div>
        <div class="mt5">
            <?php if (!$_smarty_tpl->tpl_vars['conversation']->value['multiple_recipients']) {?>
                <?php echo $_smarty_tpl->tpl_vars['conversation']->value['name_html'];?>

            <?php } else { ?>
                <span title="<?php echo $_smarty_tpl->tpl_vars['conversation']->value['name_list'];?>
"><?php echo $_smarty_tpl->tpl_vars['conversation']->value['name'];?>
</span>
            <?php }?>
        </div>
    </div>
    <div class="panel-body">
        <div class="chat-conversations js_scroller" data-slimScroll-height="367px">
            <?php echo $_smarty_tpl->getSubTemplate ('ajax.chat.conversation.messages.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        </div>
        <div class="chat-attachments attachments clearfix x-hidden">
            <ul>
                <li class="loading">
                    <div class="loader loader_small"></div>
                </li>
            </ul>
        </div>
        <div class="chat-form-container">
            <div class="x-form chat-form">
                <div class="chat-form-message">
                    <textarea class="js_autogrow  js_post-message" placeholder="<?php echo __("Write a message");?>
"></textarea>
                </div>
                <div class="x-form-tools">
                    <div class="x-form-tools-attach">
                        <i class="fa fa-camera js_x-uploader" data-handle="chat"></i>
                    </div>
                    <div class="x-form-tools-emoji js_emoji-menu-toggle">
                        <i class="fa fa-smile-o fa-lg"></i>
                    </div>
                    <?php echo $_smarty_tpl->getSubTemplate ('__emoji-menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </div>
            </div>
        </div>
    </div>
</div><?php }
}
?>