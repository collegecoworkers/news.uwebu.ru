<?php /* Smarty version 3.1.24, created on 2017-11-24 17:33:48
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/ajax.who_shares.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:7075560875a1857fc9cf3c2_74912604%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f495e276f40080866ebec1dec1eac968f3290e08' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/ajax.who_shares.tpl',
      1 => 1447623802,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7075560875a1857fc9cf3c2_74912604',
  'variables' => 
  array (
    'posts' => 0,
    'system' => 0,
    'id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a1857fc9f52a6_75947580',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a1857fc9f52a6_75947580')) {
function content_5a1857fc9f52a6_75947580 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '7075560875a1857fc9cf3c2_74912604';
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title"><?php echo __("People Who Shared This");?>
</h5>
</div>
<div class="modal-body">
    <ul>
        <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$foreach_post_Sav = $_smarty_tpl->tpl_vars['post'];
?>
        <?php echo $_smarty_tpl->getSubTemplate ('__feeds_post.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_snippet'=>true), 0);
?>

        <?php
$_smarty_tpl->tpl_vars['post'] = $foreach_post_Sav;
}
?>
    </ul>

    <?php if (count($_smarty_tpl->tpl_vars['posts']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
    <!-- see-more -->
    <div class="alert alert-info see-more js_see-more" data-get="shares" data-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
        <span><?php echo __("See More");?>
</span>
        <div class="loader loader_small x-hidden"></div>
    </div>
    <!-- see-more -->
    <?php }?>
    
</div>
<?php }
}
?>