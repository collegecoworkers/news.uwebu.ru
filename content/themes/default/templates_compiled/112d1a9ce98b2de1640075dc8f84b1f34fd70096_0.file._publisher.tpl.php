<?php /* Smarty version 3.1.24, created on 2017-11-24 16:47:01
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/_publisher.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:18648734905a184d05c735a1_42217273%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '112d1a9ce98b2de1640075dc8f84b1f34fd70096' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/_publisher.tpl',
      1 => 1511552820,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18648734905a184d05c735a1_42217273',
  'variables' => 
  array (
    '_handle' => 0,
    '_page' => 0,
    '_group' => 0,
    'system' => 0,
    '_privacy' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a184d05cae244_28594208',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a184d05cae244_28594208')) {
function content_5a184d05cae244_28594208 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '18648734905a184d05c735a1_42217273';
?>
<div class="x-form publisher" data-handle="<?php echo $_smarty_tpl->tpl_vars['_handle']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['_page']->value) {?>data-id="<?php echo $_smarty_tpl->tpl_vars['_page']->value;?>
"<?php } elseif ($_smarty_tpl->tpl_vars['_group']->value) {?>data-id="<?php echo $_smarty_tpl->tpl_vars['_group']->value;?>
"<?php }?>>

    <!-- publisher loader -->
    <div class="publisher-loader">
        <div class="loader loader_small"></div>
    </div>
    <!-- publisher loader -->

    <!-- post message -->
    <div class="relative">
        <textarea class="js_autogrow js_mention js_publisher-scraber " placeholder="<?php echo __("What is on your mind?");?>
"></textarea>
    </div>
    <!-- post message -->

    <!-- publisher scraber -->
    <div class="publisher-scraber"></div>
    <!-- publisher scraber -->

    <!-- post attachments -->
    <div class="publisher-attachments attachments clearfix x-hidden">
        <ul></ul>
    </div>
    <!-- post attachments -->

    <!-- post location -->
    <div class="publisher-meta">
        <i class="fa fa-map-marker fa-fw"></i>
        <input type="text" placeholder="<?php echo __("Where are you?");?>
">
    </div>
    <!-- post location -->

    <div class="publisher-video">
        <i class="fa fa-video-camera fa-fw"></i>
        <?php echo __("Video uploaded successfully");?>

    </div>
    
    <!-- publisher-footer -->
    <div class="publisher-footer clearfix">
        <!-- publisher-tools -->
        <ul class="publisher-tools">
            <li>
                <span class="publisher-tools-attach">
                    <i class="fa fa-camera fa-lg fa-fw js_x-uploader" data-handle="publisher" data-multiple="multiple"></i>
                </span>
            </li>
            <?php if ($_smarty_tpl->tpl_vars['system']->value['videos_enabled']) {?>
            <li>
                <span class="publisher-tools-attach">
                    <i class="fa fa-video-camera fa-lg fa-fw js_x-uploader-video"></i>
                </span>
            </li>
            <?php }?>
            <li>
                <span class="js_publisher_meta">
                    <i class="fa fa-map-marker fa-lg fa-fw"></i>
                </span>
            </li>
            <li class="relative">
                <span class="js_emoji-menu-toggle">
                    <i class="fa fa-smile-o fa-lg fa-fw"></i>
                </span>
                <?php echo $_smarty_tpl->getSubTemplate ('__emoji-menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </li>
        </ul>
        <!-- publisher-tools -->

        <!-- publisher-buttons -->
        <div class="pull-right flip mt5">
            <?php if ($_smarty_tpl->tpl_vars['_privacy']->value) {?>
            <!-- privacy -->
            <div class="btn-group" data-toggle="tooltip" data-placement="top" data-value="public" title="<?php echo __("Shared with: Public");?>
">
                <!-- <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> -->
                    <!-- <i class="btn-group-icon fa fa-globe"></i> <span class="btn-group-text hidden-xs"><?php echo __("Public");?>
</span> <span class="caret"></span> -->
                <!-- </button> -->
                <!-- <ul class="dropdown-menu" role="menu"> -->
                    <!-- <li><a href="#" data-title="<?php echo __("Shared with: Public");?>
" data-value="public"><i class="fa fa-globe"></i> <?php echo __("Public");?>
</a></li> -->
                    <!-- <li><a href="#" data-title="<?php echo __("Shared with: Folowers");?>
" data-value="friends"><i class="fa fa-users"></i> <?php echo __("Folowers");?>
</a></li> -->
                <!-- </ul> -->
            </div>
            <!-- privacy -->
            <?php }?>
            <button type="button" class="btn btn-primary js_publisher"><?php echo __("Post");?>
</button>
        </div>
        <!-- publisher-buttons -->
    </div>
    <!-- publisher-footer -->

</div><?php }
}
?>