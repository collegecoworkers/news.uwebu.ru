<?php /* Smarty version 3.1.24, created on 2017-11-24 17:18:43
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/settings.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:18105060995a1854738378f6_81406241%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '83de990b987aa3e4a6979a1f8d39301550657ce8' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/settings.tpl',
      1 => 1511554716,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18105060995a1854738378f6_81406241',
  'variables' => 
  array (
    'view' => 0,
    'system' => 0,
    'user' => 0,
    'i' => 0,
    'blocks' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a18547390b512_66619338',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a18547390b512_66619338')) {
function content_5a18547390b512_66619338 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '18105060995a1854738378f6_81406241';
echo $_smarty_tpl->getSubTemplate ('_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php echo $_smarty_tpl->getSubTemplate ('_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<!-- page content -->
<div class="container mt20">
    <div class="row">

        <div class="col-md-3 col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body with-nav">
                    <ul class="side-nav">
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active selected"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings"><i class="fa fa-wrench fa-fw fa-lg pr10"></i> <?php echo __("Account Settings");?>
</a>
                        </li>
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "profile") {?>class="active selected"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/profile"><i class="fa fa-cog fa-fw fa-lg pr10"></i> <?php echo __("Edit Profile");?>
</a>
                        </li>
                        <!-- <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "privacy") {?>class="active selected"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/privacy"><i class="fa fa-shield fa-fw fa-lg pr10"></i> <?php echo __("Privacy Settings");?>
</a>
                        </li> -->
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['social_login_enabled']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "linked") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/linked"><i class="fa fa-share-alt fa-fw fa-lg pr10"></i> <?php echo __("Linked Accounts");?>
</a>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "blocking") {?>class="active selected"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/blocking"><i class="fa fa-minus-circle fa-fw fa-lg pr10"></i> <?php echo __("Blocking");?>
</a>
                        </li> -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "delete") {?>class="active selected"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/delete"><i class="fa fa-trash fa-fw fa-lg pr10"></i> <?php echo __("Delete Account");?>
</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">

                <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                    <div class="panel-heading with-icon with-nav">
                        <!-- panel title -->
                        <div class="mb20">
                            <i class="fa fa-wrench pr5 panel-icon"></i>
                            <strong><?php echo __("Account Settings");?>
</strong>
                        </div>
                        <!-- panel title -->

                        <!-- panel nav -->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#username" data-toggle="tab">
                                    <strong class="pr5"><?php echo __("Username");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#email" data-toggle="tab">
                                    <strong class="pr5"><?php echo __("Email");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#password" data-toggle="tab">
                                    <strong class="pr5"><?php echo __("Password");?>
</strong>
                                </a>
                            </li>
                        </ul>
                        <!-- panel nav -->
                    </div>
                    <div class="panel-body tab-content">
                        <!-- username tab -->
                        <div class="tab-pane active" id="username">
                            <form class="js_ajax-forms" data-url="users/settings.php?edit=username">
                                <div class="form-group">
                                    <label for="username"><?php echo __("Username");?>
</label>
                                    <input type="text" class="form-control" name="username" id="username" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">
                                </div>
                                
                                <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- username tab -->

                        <!-- email tab -->
                        <div class="tab-pane" id="email">
                            <form class="js_ajax-forms" data-url="users/settings.php?edit=email">
                                <div class="form-group">
                                    <label for="email"><?php echo __("Email address");?>
</label>
                                    <input type="email" class="form-control" name="email" id="email" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_email'];?>
">
                                </div>

                                <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- email tab -->

                        <!-- password tab -->
                        <div class="tab-pane" id="password">
                            <form class="js_ajax-forms" data-url="users/settings.php?edit=password">
                                <div class="form-group">
                                    <label for="current"><?php echo __("Current");?>
</label>
                                    <input type="password" class="form-control" name="current" id="current">
                                </div>
                                <div class="form-group">
                                    <label for="new"><?php echo __("New");?>
</label>
                                    <input type="password" class="form-control" name="new" id="new">
                                </div>
                                <div class="form-group">
                                    <label for="confirm"><?php echo __("Re-type new");?>
</label>
                                    <input type="password" class="form-control" name="confirm" id="confirm">
                                </div>

                                <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- password tab -->
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "profile") {?>
                    <div class="panel-heading with-icon with-nav">
                        <!-- panel title -->
                        <div class="mb20">
                            <i class="fa fa-wrench pr5 panel-icon"></i>
                            <strong><?php echo __("Edit Profile");?>
</strong>
                        </div>
                        <!-- panel title -->

                        <!-- panel nav -->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#basic" data-toggle="tab">
                                    <strong class="pr5"><?php echo __("Basic");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#work" data-toggle="tab">
                                    <strong class="pr5"><?php echo __("Work");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#location" data-toggle="tab">
                                    <strong class="pr5"><?php echo __("Location");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#education" data-toggle="tab">
                                    <strong class="pr5"><?php echo __("Education");?>
</strong>
                                </a>
                            </li>
                        </ul>
                        <!-- panel nav -->
                    </div>
                    <div class="panel-body tab-content">
                        <!-- basic tab -->
                        <div class="tab-pane active" id="basic">
                            <form class="js_ajax-forms" data-url="users/settings.php?edit=basic">
                                <div class="form-group">
                                    <label for="fullname"><?php echo __("Full Name");?>
</label>
                                    <input type="text" class="form-control" name="fullname" id="fullname" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_fullname'];?>
">
                                </div>
                                <div class="form-group">
                                    <label for="gender"><?php echo __("I am");?>
</label>
                                    <select name="gender" id="gender" class="form-control">
                                        <option value="none"><?php echo __("Select Sex");?>
:</option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_gender'] == "M") {?>selected<?php }?> value="M"><?php echo __("Male");?>
</option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_gender'] == "F") {?>selected<?php }?> value="F"><?php echo __("Female");?>
</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php echo __("Birthdate");?>
</label>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <select class="form-control" name="birth_month">
                                                <option value="none"><?php echo __("Select Month");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '1') {?>selected<?php }?> value="1"><?php echo __("Jan");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '2') {?>selected<?php }?> value="2"><?php echo __("Feb");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '3') {?>selected<?php }?> value="3"><?php echo __("Mar");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '4') {?>selected<?php }?> value="4"><?php echo __("Apr");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '5') {?>selected<?php }?> value="5"><?php echo __("May");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '6') {?>selected<?php }?> value="6"><?php echo __("Jun");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '7') {?>selected<?php }?> value="7"><?php echo __("Jul");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '8') {?>selected<?php }?> value="8"><?php echo __("Aug");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '9') {?>selected<?php }?> value="9"><?php echo __("Sep");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '10') {?>selected<?php }?> value="10"><?php echo __("Oct");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '11') {?>selected<?php }?> value="11"><?php echo __("Nov");?>
</option>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '12') {?>selected<?php }?> value="12"><?php echo __("Dec");?>
</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-4">
                                            <select class="form-control" name="birth_day">
                                                <option value="none"><?php echo __("Select Day");?>
</option>
                                                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 31+1 - (1) : 1-(31)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['day'] == $_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                                <?php }} ?>
                                            </select>
                                        </div>
                                        <div class="col-xs-4">
                                            <select class="form-control" name="birth_year">
                                                <option value="none"><?php echo __("Select Year");?>
</option>
                                                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2015+1 - (1905) : 1905-(2015)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1905, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                                <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['year'] == $_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                                <?php }} ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- basic tab -->

                        <!-- work tab -->
                        <div class="tab-pane" id="work">
                            <form class="js_ajax-forms" data-url="users/settings.php?edit=work">
                                <div class="form-group">
                                    <label for="work_title"><?php echo __("Work Title");?>
</label>
                                    <input type="text" class="form-control" name="work_title" id="work_title" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_work_title'];?>
">
                                </div>
                                <div class="form-group">
                                    <label for="work_place"><?php echo __("Work Place");?>
</label>
                                    <input type="text" class="form-control" name="work_place" id="work_place" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_work_place'];?>
">
                                </div>
                                
                                <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- work tab -->

                        <!-- location tab -->
                        <div class="tab-pane" id="location">
                            <form class="js_ajax-forms" data-url="users/settings.php?edit=location">
                                
                                <div class="form-group">
                                    <label for="city"><?php echo __("Current City");?>
</label>
                                    <input type="text" class="form-control" name="city" id="city" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_current_city'];?>
">
                                </div>
                                <div class="form-group">
                                    <label for="hometown"><?php echo __("Hometown");?>
</label>
                                    <input type="text" class="form-control" name="hometown" id="hometown" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_hometown'];?>
">
                                </div>
                                
                                <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- location tab -->

                        <!-- education tab -->
                        <div class="tab-pane" id="education">
                            <form class="js_ajax-forms" data-url="users/settings.php?edit=education">
                                
                                <div class="form-group">
                                    <label for="edu_major"><?php echo __("Major");?>
</label>
                                    <input type="text" class="form-control" name="edu_major" id="edu_major" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_major'];?>
">
                                </div>
                                <div class="form-group">
                                    <label for="edu_school"><?php echo __("School");?>
</label>
                                    <input type="text" class="form-control" name="edu_school" id="edu_school" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_school'];?>
">
                                </div>
                                <div class="form-group">
                                    <label for="edu_class"><?php echo __("Class");?>
</label>
                                    <input type="text" class="form-control" name="edu_class" id="edu_class" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_class'];?>
">
                                </div>

                                <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- education tab -->
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "privacy") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-shield pr5 panel-icon"></i>
                        <strong><?php echo __("Privacy Settings");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=privacy">
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_chat">
                                    <?php echo __("Chat");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_chat" id="privacy_chat">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_chat_enabled'] == 0) {?>selected<?php }?> value="0">
                                            <?php echo __("Offline");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_chat_enabled'] == 1) {?>selected<?php }?> value="1">
                                            <?php echo __("Online");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_birthdate">
                                    <?php echo __("Who can see your");?>
 <?php echo __("birthdate");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_birthdate" id="privacy_birthdate">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_work">
                                    <?php echo __("Who can see your");?>
 <?php echo __("work info");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_work" id="privacy_work">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_work'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_work'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_location">
                                    <?php echo __("Who can see your");?>
 <?php echo __("location info");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_location" id="privacy_location">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_location'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_location'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_education">
                                    <?php echo __("Who can see your");?>
 <?php echo __("education info");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_education" id="privacy_education">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_education'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_education'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_friends">
                                    <?php echo __("Who can see your");?>
 <?php echo __("friends");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_friends" id="privacy_friends">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_friends'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_friends'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_friends'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_pages">
                                    <?php echo __("Who can see your");?>
 <?php echo __("liked pages");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_pages" id="privacy_pages">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_pages'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_pages'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_pages'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_groups">
                                    <?php echo __("Who can see your");?>
 <?php echo __("joined groups");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_groups" id="privacy_groups">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_groups'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_groups'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_groups'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-7 col-sm-offset-5">
                                    <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                </div>
                            </div>
                            
                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->

                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "linked") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-share-alt pr5 panel-icon"></i>
                        <strong><?php echo __("Linked Accounts");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['social_login_enabled']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>
                                <ul>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled']) {?>
                                    <li class="feeds-item">
                                        <div class="data-container">
                                            <div class="data-avatar">
                                                <i class="fa fa-facebook-square fa-4x" style="color: #3B579D"></i>
                                            </div>
                                            <div class="data-content">
                                                <div class="pull-right flip">
                                                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['facebook_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/facebook"><?php echo __("Disconnect");?>
</a>
                                                    <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/facebook"><?php echo __("Connect");?>
</a>
                                                    <?php }?>
                                                </div>
                                                <div>
                                                    <div class="name mt5 text-primary">
                                                        <?php echo __("Facebook");?>

                                                    </div>
                                                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['facebook_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Facebook");?>

                                                    <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Facebook");?>

                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled']) {?>
                                    <li class="feeds-item">
                                        <div class="data-container">
                                            <div class="data-avatar">
                                                <i class="fa fa-twitter-square fa-4x" style="color: #55ACEE"></i>
                                            </div>
                                            <div class="data-content">
                                                <div class="pull-right flip">
                                                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['twitter_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/twitter"><?php echo __("Disconnect");?>
</a>
                                                    <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/twitter"><?php echo __("Connect");?>
</a>
                                                    <?php }?>
                                                </div>
                                                <div>
                                                    <div class="name mt5 text-primary">
                                                        <?php echo __("Twitter");?>

                                                    </div>
                                                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['twitter_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Twitter");?>

                                                    <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Twitter");?>

                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>
                                    <li class="feeds-item">
                                        <div class="data-container">
                                            <div class="data-avatar">
                                                <i class="fa fa-google-plus-square fa-4x" style="color: #DC4A38"></i>
                                            </div>
                                            <div class="data-content">
                                                <div class="pull-right flip">
                                                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['google_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/google"><?php echo __("Disconnect");?>
</a>
                                                    <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/google"><?php echo __("Connect");?>
</a>
                                                    <?php }?>
                                                </div>
                                                <div>
                                                    <div class="name mt5 text-primary">
                                                        <?php echo __("Google");?>

                                                    </div>
                                                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['google_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Google");?>

                                                    <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Google");?>

                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php }?>
                                </ul>
                            <?php }?>
                        <?php }?>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "blocking") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-minus-circle pr5 panel-icon"></i>
                        <strong><?php echo __("Manage Blocking");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-info">
                            <?php echo __("Once you block someone, that person can no longer see things you post on your timeline");?>
<br>
                            <?php echo __("Note: Does not include apps, games or groups you both participate in");?>

                        </div>

                        <?php if (count($_smarty_tpl->tpl_vars['blocks']->value) > 0) {?>
                            <ul>
                                <?php
$_from = $_smarty_tpl->tpl_vars['blocks']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['_user'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['_user']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
$_smarty_tpl->tpl_vars['_user']->_loop = true;
$foreach__user_Sav = $_smarty_tpl->tpl_vars['_user'];
?>
                                <?php echo $_smarty_tpl->getSubTemplate ('__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_connection'=>"blocked"), 0);
?>

                                <?php
$_smarty_tpl->tpl_vars['_user'] = $foreach__user_Sav;
}
?>
                            </ul>
                        <?php } else { ?>
                            <p class="text-center text-muted">
                                <?php echo __("No blocked users");?>

                            </p>
                        <?php }?>

                        <?php if (count($_smarty_tpl->tpl_vars['blocks']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
                            <!-- see-more -->
                            <div class="alert alert-info see-more js_see-more" data-get="blocks">
                                <span><?php echo __("See More");?>
</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
                            <!-- see-more -->
                        <?php }?>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "delete") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-trash pr5 panel-icon"></i>
                        <strong><?php echo __("Delete Account");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-warning">
                            <?php echo __("Once you delete your account you will no longer can access it again");?>
<br>
                            <?php echo __("Note: All your data will be deleted");?>

                        </div>

                        <div class="text-center">
                            <button class="btn btn-danger js_delete-user"><?php echo __("Delete My Account");?>
</button>
                        </div>

                        <?php if (count($_smarty_tpl->tpl_vars['blocks']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
                            <!-- see-more -->
                            <div class="alert alert-info see-more js_see-more" data-get="blocks">
                                <span><?php echo __("See More");?>
</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
                            <!-- see-more -->
                        <?php }?>
                    </div>
                <?php }?>
                
            </div>
        </div>
        
    </div>
</div>
<!-- page content -->

<?php echo $_smarty_tpl->getSubTemplate ('_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>