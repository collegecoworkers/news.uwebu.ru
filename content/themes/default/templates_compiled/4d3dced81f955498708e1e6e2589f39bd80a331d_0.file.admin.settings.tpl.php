<?php /* Smarty version 3.1.24, created on 2017-11-24 16:18:11
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/admin.settings.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:6073767645a184643723615_03903335%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d3dced81f955498708e1e6e2589f39bd80a331d' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/admin.settings.tpl',
      1 => 1452566960,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6073767645a184643723615_03903335',
  'variables' => 
  array (
    'system' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a18464379def8_41864737',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a18464379def8_41864737')) {
function content_5a18464379def8_41864737 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '6073767645a184643723615_03903335';
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-cog pr5 panel-icon"></i>
        <strong><?php echo __("Settings");?>
</strong>
        <div class="pull-right flip">
            <small><?php echo __("System Version");?>
 (<?php echo $_smarty_tpl->tpl_vars['system']->value['version'];?>
)</small>
        </div>
    </div>
    <div class="panel-body">

        <!-- tabs nav -->
        <ul class="nav nav-tabs mb20">
            <li class="active">
                <a href="#System" data-toggle="tab">
                    <strong class="pr5"><?php echo __("System");?>
</strong>
                </a>
            </li>
            <li>
                <a href="#Registration" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Registration");?>
</strong>
                </a>
            </li>
            <li>
                <a href="#Emails" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Emails");?>
</strong>
                </a>
            </li>
            <li>
                <a href="#Security" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Security");?>
</strong>
                </a>
            </li>
            <li>
                <a href="#Limits" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Limits");?>
</strong>
                </a>
            </li>
            <li>
                <a href="#Analytics" data-toggle="tab">
                    <strong class="pr5"><?php echo __("Analytics");?>
</strong>
                </a>
            </li>
        </ul>
        <!-- tabs nav -->

        <!-- tabs content -->
        <div class="tab-content">
            <!-- System -->
            <div class="tab-pane active" id="System">
                <form class="js_ajax-forms form-horizontal" data-url="admin/settings.php?edit=system">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Website Live");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="system_live" class="onoffswitch-checkbox" id="system_live" <?php if ($_smarty_tpl->tpl_vars['system']->value['system_live']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="system_live"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn the entire website On and Off");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Shutdown Message");?>

                        </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="system_message" rows="3"><?php echo $_smarty_tpl->tpl_vars['system']->value['system_message'];?>
</textarea>
                            <span class="help-block">
                                <?php echo __("The text that is presented when the site is closed");?>

                            </span>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Website Logo");?>

                        </label>
                        <div class="col-sm-9">
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['system_logo'] == '') {?>
                            <div class="x-image">
                                <button type="button" class="close x-hidden js_x-image-remover" title="<?php echo __("Remove");?>
">
                                    <span>×</span>
                                </button>
                                <div class="loader loader_small x-hidden"></div>
                                <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                                <input type="hidden" class="js_x-image-input" name="system_logo" value="">
                            </div>
                            <?php } else { ?>
                            <div class="x-image" style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['system']->value['system_logo'];?>
')">
                                <button type="button" class="close js_x-image-remover" title="<?php echo __("Remove");?>
">
                                    <span>×</span>
                                </button>
                                <div class="loader loader_small x-hidden"></div>
                                <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                                <input type="hidden" class="js_x-image-input" name="system_logo" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_logo'];?>
">
                            </div>
                            <?php }?>
                            <span class="help-block">
                                <?php echo __("The perfect size for your logo should be (wdith: 60px & height: 46px)");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Website Title");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="system_title" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
">
                            <span class="help-block">
                                <?php echo __("Title of your website");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Website Description");?>

                        </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="system_description" rows="3"><?php echo $_smarty_tpl->tpl_vars['system']->value['system_description'];?>
</textarea>
                            <span class="help-block">
                                <?php echo __("Description of your website");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Website Domain");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="system_domain" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_domain'];?>
">
                            <span class="help-block">
                                <?php echo __("Like: sngine.com (without 'www')");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Website Path");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="system_url" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
">
                            <span class="help-block">
                                <?php echo __("The path of your system");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Uploads Directory");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="system_uploads_directory" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads_directory'];?>
">
                            <span class="help-block">
                                <?php echo __("The path of uploads directory");?>

                            </span>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Videos Enabled");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="videos_enabled" class="onoffswitch-checkbox" id="videos_enabled" <?php if ($_smarty_tpl->tpl_vars['system']->value['videos_enabled']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="videos_enabled"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn the videos On and Off");?>

                            </span>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Games Enabled");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="games_enabled" class="onoffswitch-checkbox" id="games_enabled" <?php if ($_smarty_tpl->tpl_vars['system']->value['games_enabled']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="games_enabled"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn the games On and Off");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            <!-- System -->

            <!-- Registration -->
            <div class="tab-pane" id="Registration">
                <form class="js_ajax-forms form-horizontal" data-url="admin/settings.php?edit=registration">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Registration Enabled");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="users_can_register" class="onoffswitch-checkbox" id="users_can_register" <?php if ($_smarty_tpl->tpl_vars['system']->value['users_can_register']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="users_can_register"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn registration On and Off");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Send Activation Email");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="email_send_activation" class="onoffswitch-checkbox" id="email_send_activation" <?php if ($_smarty_tpl->tpl_vars['system']->value['email_send_activation']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="email_send_activation"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Enable/Disable activation email after registration");?>

                            </span>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Social Logins");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="social_login_enabled" class="onoffswitch-checkbox" id="social_login_enabled" <?php if ($_smarty_tpl->tpl_vars['system']->value['social_login_enabled']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="social_login_enabled"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn registration/login via social media (Facebook, Twitter and etc) On and Off");?>

                            </span>
                        </div>
                    </div>

                    <!-- facebook -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Facebook Login");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="facebook_login_enabled" class="onoffswitch-checkbox" id="facebook_login_enabled" <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="facebook_login_enabled"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn registration/login via Facebook On and Off");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Facebook App ID");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="facebook_appid" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['facebook_appid'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Facebook App Secret");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="facebook_secret" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['facebook_secret'];?>
">
                        </div>
                    </div>
                    <!-- facebook -->

                    <!-- twitter -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Twitter Login");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="twitter_login_enabled" class="onoffswitch-checkbox" id="twitter_login_enabled" <?php if ($_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="twitter_login_enabled"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn registration/login via Twitter On and Off");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Twitter App ID");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="twitter_appid" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['twitter_appid'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Twitter App Secret");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="twitter_secret" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['twitter_secret'];?>
">
                        </div>
                    </div>
                    <!-- twitter -->

                    <!-- google -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Google Login");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="google_login_enabled" class="onoffswitch-checkbox" id="google_login_enabled" <?php if ($_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="google_login_enabled"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn registration/login via Google On and Off");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Google App ID");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="google_appid" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['google_appid'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Google App Secret");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="google_secret" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['google_secret'];?>
">
                        </div>
                    </div>
                    <!-- google -->

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            <!-- Registration -->

            <!-- Emails -->
            <div class="tab-pane" id="Emails">
                <form class="js_ajax-forms form-horizontal" data-url="admin/settings.php?edit=emails">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("SMTP Emails");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="email_smtp_enabled" class="onoffswitch-checkbox" id="email_smtp_enabled" <?php if ($_smarty_tpl->tpl_vars['system']->value['email_smtp_enabled']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="email_smtp_enabled"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Enable/Disable SMTP email system");?>
<br/>
                                <?php echo __("PHP mail() function will be used in case of disabled");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("SMTP Server");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="email_smtp_server" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['email_smtp_server'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("SMTP Require Authentication");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="email_smtp_authentication" class="onoffswitch-checkbox" id="email_smtp_authentication" <?php if ($_smarty_tpl->tpl_vars['system']->value['email_smtp_authentication']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="email_smtp_authentication"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Enable/Disable SMTP email system");?>
<br/>
                                <?php echo __("PHP mail() function will be used in case of disabled");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("SMTP Port");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="email_smtp_port" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['email_smtp_port'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("SMTP Username");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="email_smtp_username" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['email_smtp_username'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("SMTP Password");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="email_smtp_password" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['email_smtp_password'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            <!-- Emails -->

            <!-- Security -->
            <div class="tab-pane" id="Security">
                <form class="js_ajax-forms form-horizontal" data-url="admin/settings.php?edit=security">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("reCAPTCHA Enabled");?>

                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="reCAPTCHA_enabled" class="onoffswitch-checkbox" id="reCAPTCHA_enabled" <?php if ($_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_enabled']) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="reCAPTCHA_enabled"></label>
                            </div>
                            <span class="help-block">
                                <?php echo __("Turn reCAPTCHA On and Off");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("reCAPTCHA Site Key");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="reCAPTCHA_site_key" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_site_key'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("reCAPTCHA Secret Key");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="reCAPTCHA_secret_key" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_secret_key'];?>
">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            <!-- Security -->

            <!-- Limits -->
            <div class="tab-pane" id="Limits">
                <form class="js_ajax-forms form-horizontal" data-url="admin/settings.php?edit=limits">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Max profile photo size");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="max_avatar_size" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['max_avatar_size'];?>
">
                            <span class="help-block">
                                <?php echo __("The Maximum size of profile photo");?>
 <?php echo __("in kilobytes (1 M = 1024 KB)");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Max cover photo size");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="max_cover_size" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['max_cover_size'];?>
">
                            <span class="help-block">
                                <?php echo __("The Maximum size of cover photo");?>
 <?php echo __("in kilobytes (1 M = 1024 KB)");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Max upladed photo size");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="max_photo_size" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['max_photo_size'];?>
">
                            <span class="help-block">
                                <?php echo __("The Maximum size of uploaded photo in posts");?>
 <?php echo __("in kilobytes (1 M = 1024 KB)");?>

                            </span>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Max upladed video size");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="max_video_size" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['max_video_size'];?>
">
                            <span class="help-block">
                                <?php echo __("The Maximum size of uploaded video in posts");?>
 <?php echo __("in kilobytes (1 M = 1024 KB)");?>

                            </span>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Minimum Results");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="min_results" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['min_results'];?>
">
                            <span class="help-block">
                                <?php echo __("The Min number of results per request");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Minimum Even Results");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="min_results_even" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['min_results_even'];?>
">
                            <span class="help-block">
                                <?php echo __("The Min even number of results per request");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Maximum Results");?>

                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="max_results" value="<?php echo $_smarty_tpl->tpl_vars['system']->value['max_results'];?>
">
                            <span class="help-block">
                                <?php echo __("The Max number of results per request");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            <!-- Limits -->

            <!-- Analytics -->
            <div class="tab-pane" id="Analytics">
                <form class="js_ajax-forms form-horizontal" data-url="admin/settings.php?edit=analytics">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Google Analytics");?>

                        </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="google_analytics" rows="3"><?php echo $_smarty_tpl->tpl_vars['system']->value['google_analytics'];?>
</textarea>
                            <span class="help-block">
                                <?php echo __("Google Analytics Code");?>

                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            <!-- Analytics -->
        </div>
        <!-- tabs content -->
    </div>
</div><?php }
}
?>