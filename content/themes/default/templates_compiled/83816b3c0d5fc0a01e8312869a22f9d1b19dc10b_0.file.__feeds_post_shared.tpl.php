<?php /* Smarty version 3.1.24, created on 2017-11-24 17:33:48
         compiled from "/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/__feeds_post_shared.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:18987973545a1857fc9fcc79_04008312%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '83816b3c0d5fc0a01e8312869a22f9d1b19dc10b' => 
    array (
      0 => '/home/users/s/st-umbokc/domains/news.uwebu.ru/content/themes/default/templates/__feeds_post_shared.tpl',
      1 => 1451880650,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18987973545a1857fc9fcc79_04008312',
  'variables' => 
  array (
    'origin' => 0,
    'system' => 0,
    'photo' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5a1857fca4dbf9_71486840',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5a1857fca4dbf9_71486840')) {
function content_5a1857fca4dbf9_71486840 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '18987973545a1857fc9fcc79_04008312';
?>
<!-- post header -->
<div class="post-header">
    <!-- post picture -->
    <div class="post-avatar">
        <a class="post-avatar-picture" href="<?php echo $_smarty_tpl->tpl_vars['origin']->value['post_author_url'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['origin']->value['post_picture'];?>
);">
        </a>
    </div>
    <!-- post picture -->

    <!-- post meta -->
    <div class="post-meta">
        <!-- post author name -->
        <div>
            <!-- post author name -->
            <span class="js_user-popover" data-type="<?php echo $_smarty_tpl->tpl_vars['origin']->value['user_type'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['origin']->value['user_id'];?>
">
                <a href="<?php echo $_smarty_tpl->tpl_vars['origin']->value['post_author_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['origin']->value['post_author_name'];?>
</a>
            </span>
            <!-- post author name -->
        </div>
        <!-- post author name -->

        <!-- post time & location & privacy -->
        <div class="post-time">
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/posts/<?php echo $_smarty_tpl->tpl_vars['origin']->value['post_id'];?>
" class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['origin']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['origin']->value['time'];?>
</a>

            <?php if ($_smarty_tpl->tpl_vars['origin']->value['location']) {?>
            ·
            <i class="fa fa-map-marker"></i> <span><?php echo $_smarty_tpl->tpl_vars['origin']->value['location'];?>
</span>
            <?php }?>

            - 
            <?php if ($_smarty_tpl->tpl_vars['origin']->value['privacy'] == "friends") {?>
            <i class="fa fa-users" data-toggle="tooltip" data-placement="top" title="<?php echo __("Shared with");?>
: <?php echo __("Friends");?>
"></i>
            <?php } else { ?>
            <i class="fa fa-globe" data-toggle="tooltip" data-placement="top" title="<?php echo __("Shared with");?>
: <?php echo __("Public");?>
"></i>
            <?php }?>
        </div>
        <!-- post time & location & privacy -->
    </div>
    <!-- post meta -->
</div>
<!-- post header -->

<!-- post text -->
<div class="post-text text-muted"><?php echo $_smarty_tpl->tpl_vars['origin']->value['text'];?>
</div>
<!-- post text -->

<?php if ($_smarty_tpl->tpl_vars['origin']->value['post_type'] == "avatar") {?>
    <div class="pg_wrapper">
        <div class="pg_1x">
            <a href="#" class="js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['user_picture'];?>
">
                <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['user_picture'];?>
">
            </a>
        </div>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['post_type'] == "cover") {?>
    <div class="pg_wrapper">
        <div class="pg_1x">
            <a href="#" class="js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['user_cover'];?>
">
                <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['user_cover'];?>
">
            </a>
        </div>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['post_type'] == "photos" && $_smarty_tpl->tpl_vars['origin']->value['photos']) {?>
    <div class="mt10 clearfix">
        <div class="pg_wrapper">
            <?php if ($_smarty_tpl->tpl_vars['origin']->value['photos_num'] == 1) {?>
            <div class="pg_1x">
                <a href="#" class="js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['source'];?>
">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['source'];?>
">
                </a>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['photos_num'] == 2) {?>
            <?php
$_from = $_smarty_tpl->tpl_vars['origin']->value['photos'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['photo']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->value) {
$_smarty_tpl->tpl_vars['photo']->_loop = true;
$foreach_photo_Sav = $_smarty_tpl->tpl_vars['photo'];
?>
            <div class="pg_2x">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['photo']->value['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['photo']->value['source'];?>
');"></a>
            </div>
            <?php
$_smarty_tpl->tpl_vars['photo'] = $foreach_photo_Sav;
}
?>
            <?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['photos_num'] == 3) {?>
            <div class="pg_3x">
                <div class="pg_2o3">
                    <div class="pg_2o3_in">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['source'];?>
');"></a>
                    </div>
                </div>
                <div class="pg_1o3">
                    <div class="pg_1o3_in">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][1]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][1]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][1]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][1]['source'];?>
');"></a>
                    </div>
                    <div class="pg_1o3_in">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][2]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][2]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][2]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][2]['source'];?>
');"></a>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="pg_4x">
                <div class="pg_2o3">
                    <div class="pg_2o3_in">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][0]['source'];?>
');"></a>
                    </div>
                </div>
                <div class="pg_1o3">
                    <div class="pg_1o3_in">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][1]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][1]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][1]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][1]['source'];?>
');"></a>
                    </div>
                    <div class="pg_1o3_in">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][2]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][2]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][2]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][2]['source'];?>
');"></a>
                    </div>
                    <div class="pg_1o3_in">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/photos/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][3]['photo_id'];?>
" class="js_lightbox" data-id="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][3]['photo_id'];?>
" data-image="<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][3]['source'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos'][3]['source'];?>
');">
                            <?php if ($_smarty_tpl->tpl_vars['origin']->value['photos_num'] > 4) {?>
                            <span class="more">+<?php echo $_smarty_tpl->tpl_vars['origin']->value['photos_num']-4;?>
</span>
                            <?php }?>
                        </a>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['post_type'] == "media") {?>
    <div class="mt10">
        <?php if ($_smarty_tpl->tpl_vars['origin']->value['media']['media_type'] == "youtube") {?>
        <div class="post-media">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_uid'];?>
" allowfullscreen=""></iframe>
            </div>
            <div class="post-media-meta">
                <a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['soucre_title'];?>
</a>
                <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_text'];?>
</div>
                <div class="source">youtube.com</div>
            </div>
        </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['media']['media_type'] == "vimeo") {?>
        <div class="post-media">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="http://player.vimeo.com/video/<?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_uid'];?>
"></iframe>
            </div>
            <div class="post-media-meta">
                <a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['soucre_title'];?>
</a>
                <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_text'];?>
</div>
                <div class="source">vimeo.com</div>
            </div>
        </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['media']['media_type'] == "soundcloud") {?>
        <div class="post-media">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe height="450" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_uid'];?>
&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
            </div>
            <div class="post-media-meta">
                <a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['soucre_title'];?>
</a>
                <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['origin']->value['media']['source_text'];?>
</div>
                <div class="source">soundcloud.com</div>
            </div>
        </div>
        <?php }?>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['post_type'] == "link") {?>
    <?php if ($_smarty_tpl->tpl_vars['origin']->value['link']['link_title'] || $_smarty_tpl->tpl_vars['origin']->value['link']['link_text'] || $_smarty_tpl->tpl_vars['origin']->value['link']['link_host']) {?>
    <div class="mt10">
        <div class="post-media">
            <?php if ($_smarty_tpl->tpl_vars['origin']->value['link']['link_thumbnail']) {?>
            <div class="post-media-image">
                <div style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['origin']->value['link']['link_thumbnail'];?>
');"></div>
            </div>
            <?php }?>
            <div class="post-media-meta">
                <a class="title mb5" href="<?php echo $_smarty_tpl->tpl_vars['origin']->value['link']['link_url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['origin']->value['link']['link_title'];?>
</a>
                <div class="text mb5"><?php echo $_smarty_tpl->tpl_vars['origin']->value['link']['link_text'];?>
</div>
                <div class="source"><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['origin']->value['link']['link_host'], 'UTF-8');?>
</div>
            </div>
        </div>
    </div>
    <?php }?>
<?php } elseif ($_smarty_tpl->tpl_vars['origin']->value['post_type'] == "video" && $_smarty_tpl->tpl_vars['origin']->value['video']) {?>
    <video controls>
        <source src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['origin']->value['video']['source'];?>
" type="video/mp4">
        <?php echo __("Your browser does not support HTML5 video");?>

    </video>
<?php }
}
}
?>